﻿#include <iostream>

const int LimitNumber = 10;

void EvenOrOddNumbers(int StartNumber, int LimitNumber)
{

	for (StartNumber; StartNumber <= LimitNumber; StartNumber += 2)
	{
		std::cout << StartNumber << std::endl;
	}

}


int main()
{
	std::cout << "Even numbers: " << std::endl;

	EvenOrOddNumbers(0, LimitNumber);

	std::cout << "Choose: " << std::endl <<
		"1. Even Numbers" << std::endl <<
		"2. Odd Numbers" << std::endl;

	int ChooseNumber;
	std::cin >> ChooseNumber;

	switch (ChooseNumber)
	{
	case 1:
		std::cout << "Even numbers: " << std::endl;
		EvenOrOddNumbers(0, LimitNumber);
		break;


	case 2:
		std::cout << "Odd numbers: " << std::endl;
		EvenOrOddNumbers(1, LimitNumber);
		break;

	default:
		break;
	}

}